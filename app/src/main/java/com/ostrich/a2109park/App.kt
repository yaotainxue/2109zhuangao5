package com.ostrich.a2109park

import android.app.Application
import android.util.Log
import com.tencent.smtt.sdk.QbSdk


/**
 * @Author : yaotianxue
 * @Time : On 2024/3/22 13:35
 * @Description : App
 */
class App:Application() {
    override fun onCreate() {
        super.onCreate()
        val cb = object : QbSdk.PreInitCallback {
            override fun onViewInitFinished(arg0: Boolean) {
                // TODO Auto-generated method stub
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Log.d("app", " onViewInitFinished is $arg0")
            }

            override fun onCoreInitFinished() {
                // TODO Auto-generated method stub
            }
        }
        //X5初始化
        QbSdk.initX5Environment(this,null)
    }
}