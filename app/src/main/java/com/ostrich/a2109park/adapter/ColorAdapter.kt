package com.ostrich.a2109park.adapter

import android.graphics.Color
import android.util.Log
import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ostrich.a2109park.R
import com.ostrich.a2109park.mvvm.model.entity.CarColorValues

/**
 * @Author : yaotianxue
 * @Time : On 2024/4/1 10:07
 * @Description : ColorAdapter
 */
class ColorAdapter:BaseQuickAdapter<CarColorValues,BaseViewHolder>(R.layout.item_color) {
    companion object{
        const val TYPE = 1
    }
    override fun convert(helper: BaseViewHolder, item: CarColorValues?) {
        Log.e("ZXY", "ColorAdapter:$itemCount")
        item.let {
            val img = helper.getView<ImageView>(R.id.item_color_img)
            img.setBackgroundColor(Color.parseColor(it?.colorValue))
        }

    }
}