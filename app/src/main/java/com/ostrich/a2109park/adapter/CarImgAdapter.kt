package com.ostrich.a2109park.adapter

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ostrich.a2109park.R
import java.io.File

/**
 * @Author : yaotianxue
 * @Time : On 2024/4/1 11:06
 * @Description : CarImgAdapter
 */
class CarImgAdapter(list:ArrayList<String>): BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_carimg,list) {
    companion object{
        const val TYPE = 2
    }
    override fun convert(helper: BaseViewHolder, item: String?) {
        if(item == "+"){
            helper.setImageResource(R.id.item_car_img,R.drawable.add)
        }else{
            Glide.with(mContext)
                .load(File(item))
                .into(helper.getView<View>(R.id.item_car_img) as ImageView)
        }
    }
}