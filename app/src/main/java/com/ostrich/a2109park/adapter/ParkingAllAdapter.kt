package com.ostrich.a2109park.adapter

import android.graphics.Color
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ostrich.a2109park.R
import com.ostrich.a2109park.mvvm.model.entity.ParkingAllValues

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 14:21
 * @Description : ParkingAllAdapter
 */
class ParkingAllAdapter(private val layoutId:Int, private val list:MutableList<ParkingAllValues>): BaseQuickAdapter<ParkingAllValues,BaseViewHolder>(layoutId,list) {
    override fun convert(helper: BaseViewHolder, item: ParkingAllValues?) {
        item?.let {
            helper.setText(R.id.parkingall_item_tv,it.parkName)
            if(it.parkUser == 0){
                //可选择
                helper.getView<View>(R.id.parkingall_item_tv)
                    .setBackgroundColor(Color.parseColor("#8fdb0a"))
            }else{
                //不可选择
                helper.getView<View>(R.id.parkingall_item_tv)
                    .setBackgroundColor(Color.GRAY)
            }
            if(it.leftFlag){
                helper.getView<View>(R.id.parkingall_item_tv)
                    .setBackgroundColor(Color.WHITE)
                helper.setTextColor(R.id.parkingall_item_tv,Color.GRAY)
            }

        }

    }
}