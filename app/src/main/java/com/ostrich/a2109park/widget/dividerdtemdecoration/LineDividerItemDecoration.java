package com.ostrich.a2109park.widget.dividerdtemdecoration;

import android.content.Context;

import com.yanyusong.y_divideritemdecoration.Y_Divider;
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder;
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration;

import androidx.annotation.Nullable;

public class LineDividerItemDecoration extends Y_DividerItemDecoration {
    public LineDividerItemDecoration(Context context) {
        super(context);
    }

    @Nullable
    @Override
    public Y_Divider getDivider(int itemPosition) {
        Y_Divider divider = null;
        divider = new Y_DividerBuilder()
                .setBottomSideLine(true, 0xff333333, 1, 0, 0)
                .create();
        return divider;
    }
}
