package com.ostrich.a2109park.widget;

public interface WebViewJavaScriptFunction {

	void onJsFunctionCalled(String tag);
}
