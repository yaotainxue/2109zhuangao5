package com.ostrich.a2109park.mvc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.SPStaticUtils;
import com.ostrich.a2109park.R;
import com.ostrich.a2109park.dao.LoginUserDao;
import com.ostrich.a2109park.mvvm.model.entity.LoginValues;
import com.ostrich.a2109park.mvvm.ui.activity.ParkingAllActivity;
import com.ostrich.a2109park.widget.WebViewJavaScriptFunction;
import com.ostrich.a2109park.widget.X5WebView;
import com.ostrich.common.network.HttpRetrofitFactory;
import com.ostrich.common.network.HttpType;
/**
 * 软件主界面->显示服务器返回的浏览器界面
 * */
public class MainActivity extends AppCompatActivity {
    private String url;
    private X5WebView main_x5web;
    private MainWebViewJavaScriptFunction listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //主页面网址
        url = SPStaticUtils.getString("home");
        init();
        //动态获取权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.READ_SMS
            },101);
        }


    }

    private void init(){
        listener = new MainWebViewJavaScriptFunction();
        main_x5web = findViewById(R.id.main_x5view);
        main_x5web.loadUrl(url);//加载首页
        main_x5web.getView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        main_x5web.addJavascriptInterface(listener,"androidinfo");//listener监听器,s:要监听的名称->h5开发人员进行约定

    }
    private void start(Class clz){
        Intent intent = new Intent(this,clz);
        startActivity(intent);
    }


    private class MainWebViewJavaScriptFunction implements WebViewJavaScriptFunction {

        @Override
        public void onJsFunctionCalled(String tag) {
        }
        //注册要监听的js事件
        @JavascriptInterface
        public void androidparking(String id){
            Log.e("ZXY","androidparking:"+id);
            start(ParkingAllActivity.class);
        }
        @JavascriptInterface
        public void androidapply(String id){
            Log.e("ZXY","androidapply:"+id);
//            start(ApplyActivity.class);
        }
        @JavascriptInterface
        public void androidvisit(String id){
//            start(VisitorActivity.class);
            Log.e("ZXY","androidvisit:"+id);
        }
        @JavascriptInterface
        public void androidpatrol(String id){
            Log.e("ZXY","androidpatrol:"+id);
//            start(PatrolesActivity.class);
        }
        @JavascriptInterface
        public void androidnews(String id){
            Log.e("ZXY","新闻androidnews:"+id);
//            start(NewsActivity.class);
        }
        @JavascriptInterface
        public void androidnoticelist(String id){
//            start(NoticeActivity.class);
            Log.e("ZXY","公告列表:"+id);
        }
        @JavascriptInterface
        public void androidrepair(String id){
            Log.e("ZXY","androidrepair:"+id);
//            start(RepairActivity.class);
        }
        @JavascriptInterface
        public void androidaddrepair(String id){
//            start(AddRepairActivity.class);
            Log.e("ZXY","androidaddrepair:"+id);
        }
        @JavascriptInterface
        public void androidaddculture(String id){
            //添加文化
//            start(AddNewsActivity.class);
            Log.e("ZXY","androidaddculture:"+id);
        }
        @JavascriptInterface
        public void androidaddnotice(String id){
//            start(AddNoticeActivity.class);
            //发公告
            Log.e("ZXY","androidaddnotice:"+id);
        }
        @JavascriptInterface
        public void androidculture(String id){
            //文化审核
//            start(NewsAdminActivity.class);
            Log.e("ZXY","androidculture:"+id);
        }
        @JavascriptInterface
        public void androidnotice(String id){
//            start(NoticeActivity.class);
            //公告管理
            Log.e("ZXY","androidnotice:"+id);
        }
        @JavascriptInterface
        public void androidsign(String id){
            Log.e("ZXY","androidsign:"+id);
//            start(SysSignActivity.class);
        }
        @JavascriptInterface
        public void androidattendance(String id){
            Log.e("ZXY","androidattendance:"+id);
//            start(HistorySignActivity.class);
        }
        @JavascriptInterface
        public void androidproperty(String id){
            Log.e("ZXY","androidproperty:"+id);
//            start(PropertyVerifyActivity.class);
        }
        @JavascriptInterface
        public void androidcheckculture(String id){
            //文化审核
//            start(VerifyNewsActivity.class);
            Log.e("ZXY","androidcheckculture:"+id);
        }
        @JavascriptInterface
        public void androidpeople(String id){
//            start(DepartmentActivity.class);
            Log.e("ZXY","androidpeople:"+id);
        }
    }
}