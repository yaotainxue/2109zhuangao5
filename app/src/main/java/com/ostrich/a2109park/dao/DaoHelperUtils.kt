package com.ostrich.a2109park.dao

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ostrich.a2109park.mvvm.model.entity.LoginEntity
import com.ostrich.a2109park.mvvm.model.entity.LoginValues
import com.ostrich.common.entity.BaseEntity

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/22 09:07
 * @Description : DaoHelperUtils
 */
class DaoHelperUtils(
    context: Context?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int,
) : SQLiteOpenHelper(context, "park.db", factory, version) {

    override fun onCreate(db: SQLiteDatabase?) {
        //用户持久化存储表
        db?.execSQL("create table sys_user (uId integer not null primary key," +
                "pId integer not null," +
                "uName text not null," +
                "uPwd text not null," +
                "time text not null," +
                "token text not null );")
    }

    /**
     * 版本升级会执行该方法
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        try {
            db?.execSQL("DROP TABLE IF EXISTS sys_user")//删除表
            onCreate(db)//重新创建表
        } catch (e: Exception) {
        }
    }
    fun insert(table:String,values:ContentValues):Int{
        val db = writableDatabase ?: return -1
        val index = db.insert(table, null, values)
        db.close()
        return index.toInt()
    }
    fun delete(table: String,whereClause:String,whereArgs: Array<String>):Int{
        val db = writableDatabase ?: return -1
        val index = db.delete(table, whereClause, whereArgs)
        db.close()
        return index
    }

    fun update(table: String,values: ContentValues,whereClause:String,whereArgs: Array<String>):Int{
        val db: SQLiteDatabase = writableDatabase?: return -1
        db.update(table, values, whereClause, whereArgs)
        return 0
    }

//    fun selAll(sqlStr:String):List<BaseEntity>{
//        val db: SQLiteDatabase = readableDatabase
//        db.
//
//
//
//    }

    @SuppressLint("Range")
    fun selUser(sqlStr:String):LoginValues{
        val db: SQLiteDatabase = readableDatabase
        val cursor = db.rawQuery(sqlStr, null)
        val loginValues = LoginValues()
        while (cursor.moveToNext()){
            val uId = cursor.getInt(cursor.getColumnIndex("uId"))
            val pId = cursor.getInt(cursor.getColumnIndex("pId"))
            val uName = cursor.getString(cursor.getColumnIndex("uName"))
            val uPwd = cursor.getString(cursor.getColumnIndex("uPwd"))
            val time = cursor.getString(cursor.getColumnIndex("time"))
            val token = cursor.getString(cursor.getColumnIndex("token"))
            loginValues.uId = uId
            loginValues.pId = pId
            loginValues.uName = uName
            loginValues.uPwd = uPwd
            loginValues.time = time
            loginValues.token = token
        }
        cursor.close()
        db.close()
        return loginValues
    }



}