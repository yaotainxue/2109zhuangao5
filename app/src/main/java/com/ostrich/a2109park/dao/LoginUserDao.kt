package com.ostrich.a2109park.dao

import android.content.ContentValues
import android.content.Context
import android.text.TextUtils
import com.ostrich.a2109park.mvvm.model.entity.LoginValues

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/22 13:15
 * @Description : LoginUserDao
 */
class LoginUserDao private constructor(context: Context){
    private val utils:DaoHelperUtils = DaoHelperUtils(context, null, 1)
    //伴生对象
    companion object {
        @Volatile
        private var instance: LoginUserDao? = null

        @JvmStatic
        fun getInstance(context: Context): LoginUserDao {
            return instance ?: synchronized(this) {
                instance ?: LoginUserDao(context).also { instance = it }
            }
        }
    }

    /**
     * 添加用户
     */
    fun insert(values:LoginValues):Int{
        if(values.uId < 0){
            return -1
        }
        if(values.pId < 0){
            return -1
        }
        if(TextUtils.isEmpty(values.uName)){
            return -1
        }
        if(TextUtils.isEmpty(values.uPwd)){
            return -1
        }
        if(TextUtils.isEmpty(values.token)){
            return -1
        }
        if(TextUtils.isEmpty(values.time)){
            return -1
        }

        val contentValues = ContentValues()
        contentValues.put("uId",values.uId)
        contentValues.put("pId",values.pId)
        contentValues.put("uName",values.uName)
        contentValues.put("uPwd",values.uPwd)
        contentValues.put("time",values.time)
        contentValues.put("token",values.token)
        return utils.insert("sys_user",contentValues)
    }

    /**
     * 删除用户
     */
    fun deleteUser(uId:Int):Int{
      return  utils.delete("sys_user","uId=?",arrayOf(uId.toString()))
    }

    /**
     * 修改用户
     */
    fun updateUser(values:LoginValues):Int{
        val contentValues = ContentValues()
        contentValues.put("uId",values.uId)
        contentValues.put("pId",values.pId)
        contentValues.put("uName",values.uName)
        contentValues.put("uPwd",values.uPwd)
        contentValues.put("time",values.time)
        contentValues.put("token",values.token)
        return utils.update("sys_user",contentValues,"uId=?",arrayOf(values.uId.toString()))
    }

    /**
     * 根据id查询用户
     */
    fun selUser(uId:Int):LoginValues{
        return utils.selUser("select * from sys_user where uId=$uId")
    }

    /**
     * 根据id查询用户
     */
    fun selUser():LoginValues{
        return utils.selUser("select * from sys_user ")
    }

    /**
     * 查询所有用户
     */
    fun selAllUser():LoginValues{
        return utils.selUser("select * from sys_user")
    }




}