package com.ostrich.a2109park.bind;

import java.util.ArrayList;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class GridVerticalAdapterEntity<T> extends BaseObservable {

    private int spanCount;//网格布局列数
    private int orientation;//线性方向
    private int layoutId;//适配器关联的xml ID
    private int type;//要构建的适配器
    private ArrayList<T> datas;//适配器关联的数据源

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSpanCount() {
        return spanCount;
    }

    public void setSpanCount(int spanCount) {
        this.spanCount = spanCount;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    @Bindable
    public ArrayList<T> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<T> data) {
        this.datas=data;
        notifyChange();
    }
}
