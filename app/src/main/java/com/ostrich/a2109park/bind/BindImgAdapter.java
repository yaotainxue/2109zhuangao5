package com.ostrich.a2109park.bind;

import android.graphics.Color;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import androidx.databinding.BindingAdapter;

/**
 * ImageView的基本自定义属性设置
 * */
public class BindImgAdapter {

    //colorbg->RGB颜色值
    @BindingAdapter("colorbg")
    public static void setColorBg(ImageView imageView,String colorbg){
        if (colorbg == null)
            return;
        if (colorbg.equals(""))
            return;
        imageView.setBackgroundColor(Color.parseColor(colorbg));
    }

    public int getMinute(int first){
        int result = 0;
        String fStr = ""+first;
        char[] fChars = fStr.toCharArray();
        Integer [] integers = new Integer[fChars.length];
        for (int i = 0;i < fChars.length;i ++){
            integers[i] = Integer.valueOf(""+fChars[i]);
        }
        ArrayList<Integer>list = new ArrayList<Integer>(Arrays.asList(integers));
        Collections.sort(list);
        int max = list.get(list.size()-1);
        int maxIndex = -1;
        for (int i = 0;i < integers.length;i ++){
            if (integers[i]==max)
                maxIndex = i;
        }
        integers[maxIndex] = integers[0];
        integers[0] = max;
        StringBuffer stringBuffer = new StringBuffer();
        for (Integer i : integers){
            stringBuffer.append(i);
        }
        result = Integer.valueOf(stringBuffer.toString());
        return result;
    }

}
