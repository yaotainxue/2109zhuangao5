package com.ostrich.a2109park.bind

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ostrich.a2109park.adapter.CarImgAdapter
import com.ostrich.a2109park.adapter.ColorAdapter
import com.ostrich.a2109park.adapter.ParkingAllAdapter
import com.ostrich.a2109park.mvvm.model.entity.CarColorValues
import com.ostrich.a2109park.mvvm.model.entity.ParkingAllValues
import com.ostrich.a2109park.mvvm.viewmodel.ApplyParkingViewModel
import com.ostrich.a2109park.widget.dividerdtemdecoration.ParkDividerItemDecoration

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 14:30
 * @Description : BindRVAdapter
 */
 class BindRVAdapter {
    companion object{

        @JvmStatic
        @BindingAdapter(value = ["entity","divider","listener"], requireAll = false)
        fun bindRecycler(recyclerView: RecyclerView,entity:GridVerticalAdapterEntity<ParkingAllValues>,divider:Int,listener:BaseQuickAdapter.OnItemClickListener ){
            //获取与recyclerview关联的适配器->null第一次进入;!null数据改变进入
            var adapter = recyclerView.adapter
            if(adapter == null){
                adapter = ParkingAllAdapter(entity.layoutId,entity.datas as MutableList<ParkingAllValues>)
                adapter.onItemClickListener = listener
                recyclerView.layoutManager = GridLayoutManager(recyclerView.context,entity.spanCount)
                recyclerView.addItemDecoration(ParkDividerItemDecoration(recyclerView.context))
                recyclerView.adapter = adapter
            }else{
                adapter = adapter as ParkingAllAdapter
                adapter.setNewData(entity.datas as MutableList<ParkingAllValues>)
            }
        }

        /**
         * listener：item点击
         * type：类型
         * datas：数据源
         * childlister：item子控件的点击
         */
        @JvmStatic
        @BindingAdapter(value = ["h_listener","type","datas","childlister"], requireAll = false)
        fun bindHRecycler(recyclerView: RecyclerView, h_listener:BaseQuickAdapter.OnItemClickListener, type:Int, datas: ObservableField<ArrayList<Any>>, childlister:BaseQuickAdapter.OnItemChildClickListener?){
            //获取与recyclerview关联的适配器->null第一次进入;!null数据改变进入
            var adapter = recyclerView.adapter
            if(adapter == null){
                if(type == ColorAdapter.TYPE ){
                    adapter = ColorAdapter()
                    adapter.onItemClickListener = h_listener
                }else if(type == CarImgAdapter.TYPE){
                    adapter = CarImgAdapter(datas.get() as ArrayList<String>)
                    adapter.onItemClickListener = h_listener
                }

                val linearLayoutManager = LinearLayoutManager(recyclerView.context)
                linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
                recyclerView.layoutManager = linearLayoutManager
                recyclerView.adapter = adapter
            }else{
                if(type == ColorAdapter.TYPE){
                    adapter = adapter as ColorAdapter
                    adapter.setNewData(datas.get() as ArrayList<CarColorValues>)
                }else  if(type == CarImgAdapter.TYPE){
                    adapter = adapter as CarImgAdapter
                    adapter.setNewData(datas.get() as ArrayList<String>)
                }
            }
        }

    }
}