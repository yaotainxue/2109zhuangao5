package com.ostrich.a2109park.mvvm.model.entity

import com.google.gson.Gson
import com.ostrich.common.entity.BaseEntity

/**
 * @Author : wjs
 * @Time : On 2024/3/22 09:30
 * @Description : LoginEntity
 */
data class LoginEntity(
    val home: String,
    val values: String,
    var loginValues:LoginValues

):BaseEntity(){
    fun getValues(): LoginValues {
        if(values != null){
            loginValues = Gson().fromJson(values, LoginValues::class.java)
        }
        return loginValues
    }
}
data class LoginValues(
    var dept_id: Int = 0,
    var pId: Int = 0,
    var time: String = "",
    var token: String = "",
    var uId: Int = 0,
    var uName: String = "",
    var uPwd: String = "",
)