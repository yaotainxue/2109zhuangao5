package com.ostrich.a2109park.mvvm.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ostrich.a2109park.R
import com.ostrich.a2109park.BR
import com.ostrich.a2109park.databinding.ActivityParkingAllBinding
import com.ostrich.a2109park.mvvm.viewmodel.ParkingAllViewModel
import com.ostrich.common.mvvm.base.BaseActivity

class ParkingAllActivity : BaseActivity<ActivityParkingAllBinding, ParkingAllViewModel>() {
    override fun initLayout(): Int {
        return R.layout.activity_parking_all
    }

    override fun initView() {

    }

    override fun initVariable(): Int {
        return BR.vm
    }

}