package com.ostrich.a2109park.mvvm.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.ostrich.a2109park.R
import com.ostrich.a2109park.bind.GridVerticalAdapterEntity
import com.ostrich.a2109park.mvc.ParkingValuesActivity
import com.ostrich.a2109park.mvvm.model.ParkingAllModel
import com.ostrich.a2109park.mvvm.model.entity.ParkingAllEntity
import com.ostrich.a2109park.mvvm.model.entity.ParkingAllValues
import com.ostrich.a2109park.widget.dividerdtemdecoration.ParkDividerItemDecoration
import com.ostrich.common.entity.BaseEntity
import com.ostrich.common.filed.ActivityField
import com.ostrich.common.mvvm.base.BaseOBViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/26 11:01
 * @Description : ParkingAllViewModel
 */
class ParkingAllViewModel(application: Application) :BaseOBViewModel<ParkingAllModel>(application){
    private val TAG = "ParkingAllViewModel"
    val entity:GridVerticalAdapterEntity<ParkingAllValues>
    val listener:ParkingItemListener
    val divider:Int = ParkDividerItemDecoration.ID //指定使用那个分隔线
    var resultFlag = false
    init{
        entity = GridVerticalAdapterEntity<ParkingAllValues>()
        entity.layoutId = R.layout.item_parkingall
        entity.spanCount = 6
        entity.type = 0
        entity.datas = ArrayList<ParkingAllValues>()
        listener = ParkingItemListener()
    }

    override fun createModel(): ParkingAllModel {
        return ParkingAllModel()
    }

    override fun initData() {
        m.request()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this)

    }

    override fun onNext(t: BaseEntity) {
        if(t.statuesCode == "200"){
            val parkingAllEntity = t as ParkingAllEntity
            val data = parkingAllEntity.getValues()
            Log.d(TAG, "onNext: $data")
            //请求数据成功展示数据
            createLeft(data)
        }
    }

    private fun createLeft(data: List<ParkingAllValues>) {
        val allList = ArrayList<ParkingAllValues>()
        allList.addAll(data)
        //计算总行数
        val line = if(data.size % entity.spanCount - 1 == 0) data.size / entity.spanCount else data.size / entity.spanCount+1
        for(i in 0 until line){
            val values = ParkingAllValues(leftFlag = true, parkName = "第" + (i + 1) + "行")
            allList.add(i*6,values)
        }
        //刷新数据
        entity.datas = allList
    }

    inner class ParkingItemListener:BaseQuickAdapter.OnItemClickListener{
        override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
            val entity: ParkingAllValues =
                adapter!!.getItem(position) as ParkingAllValues
            if (!resultFlag) {
                val map: MutableMap<String, Any> = HashMap()
                map[ActivityField.ACTCLAZZ] = ParkingValuesActivity::class.java
                val bundle = Bundle()
                bundle.putInt(ActivityField.ACTBUNDLE, entity.parkId)
                bundle.putString("parkName", entity.parkName)
                map[ActivityField.ACTBUNDLE] = bundle
                startAct(map)
            } else {
//                EventBus.getDefault().post(entity)
            }
        }

    }

}