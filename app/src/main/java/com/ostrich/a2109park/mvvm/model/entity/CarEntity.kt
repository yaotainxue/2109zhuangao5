package com.ostrich.a2109park.mvvm.model.entity

import com.google.gson.Gson
import com.ostrich.common.entity.BaseEntity

/**
 * @Author : wjs
 * @Time : On 2024/4/1 15:20
 * @Description : CarEntity
 */
data class CarEntity(val values:String,var carValues:CarValues ):BaseEntity(){
    fun getValues(): CarValues {
        if(values != null){
            carValues = Gson().fromJson(values, CarValues::class.java)
        }
        return carValues
    }

}
data class CarValues(val carId:String)