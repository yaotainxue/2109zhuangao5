package com.ostrich.a2109park.mvvm.model.request_entity;

import androidx.databinding.BaseObservable;

/**
 * BaseObservable :DataBinding数据双向绑定
 */
public class UserEntity extends BaseObservable {

    private String userName;//用户名
    private String userPwd;//用户密码

    public UserEntity(String userName,String userPwd){
        this.userName = userName;
        this.userPwd = userPwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        notifyChange();
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
        notifyChange();
    }
}
