package com.ostrich.a2109park.mvvm.model.entity

import com.ostrich.common.entity.BaseEntity

/**
 * @Author : wjs
 * @Time : On 2024/4/1 15:10
 * @Description : UploadImagesEntity
 */
class UploadImagesEntity():BaseEntity()
