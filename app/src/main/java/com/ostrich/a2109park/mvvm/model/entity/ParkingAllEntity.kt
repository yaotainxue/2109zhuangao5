package com.ostrich.a2109park.mvvm.model.entity

import com.google.gson.Gson
import com.ostrich.common.entity.BaseEntity
import org.json.JSONArray
import org.json.JSONException

/**
 * @Author : wjs
 * @Time : On 2024/3/29 14:03
 * @Description : ParkingAllEntity
 */
data class ParkingAllEntity(
    val values: String,
    var list: MutableList<ParkingAllValues> ,
):BaseEntity(){
    fun getValues(): List<ParkingAllValues> {
        if (list == null) {
            list = mutableListOf()
            if (values == "[]") return list
            try {
                val jay = JSONArray(values)
                for (i in 0 until jay.length()) {
                    val v: ParkingAllValues =
                        Gson().fromJson(jay.getJSONObject(i).toString(), ParkingAllValues::class.java)
                    list.add(v)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return list
    }
}
data class ParkingAllValues(
    val parkId: Int = 0,
    val parkName: String = "",
    val parkStart: String = "",
    val parkTime: String = "",
    val parkUser: Int = 0,
    var leftFlag: Boolean = false,
)


