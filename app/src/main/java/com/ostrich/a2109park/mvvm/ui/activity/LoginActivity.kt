package com.ostrich.a2109park.mvvm.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ostrich.a2109park.R
import com.ostrich.a2109park.BR
import com.ostrich.a2109park.databinding.ActivityLoginBinding
import com.ostrich.a2109park.mvvm.viewmodel.LoginViewModel
import com.ostrich.common.mvvm.base.BaseActivity

class LoginActivity : BaseActivity<ActivityLoginBinding,LoginViewModel>() {
    override fun initLayout(): Int {
        return R.layout.activity_login;
    }

    override fun initView() {
        v.entity = vm.entity
        v.listener = vm.listener
    }

    override fun initVariable(): Int {
       return BR.vm
    }

}