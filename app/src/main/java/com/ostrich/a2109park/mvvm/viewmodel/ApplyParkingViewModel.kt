package com.ostrich.a2109park.mvvm.viewmodel

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import com.blankj.utilcode.util.TimeUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.google.gson.Gson
import com.ostrich.a2109park.adapter.CarImgAdapter
import com.ostrich.a2109park.adapter.ColorAdapter
import com.ostrich.a2109park.mvvm.model.ApplyParkingModel
import com.ostrich.a2109park.mvvm.model.entity.AddSysParkingEntity
import com.ostrich.a2109park.mvvm.model.entity.CarColorEntity
import com.ostrich.a2109park.mvvm.model.entity.CarEntity
import com.ostrich.a2109park.mvvm.model.entity.LoginValues
import com.ostrich.common.entity.BaseEntity
import com.ostrich.common.mvvm.base.BaseOBViewModel
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat

/**
 * @Author : yaotianxue
 * @Time : On 2024/4/1 09:14
 * @Description : ApplyParkingViewModel
 */
class ApplyParkingViewModel(application: Application): BaseOBViewModel<ApplyParkingModel>(application) {
    var timeStr //车位使用的时间
            : ObservableField<String>? = null
    var applyTimeStr //车位申请时间
            : ObservableField<String>? = null
    var nameStr //用户名
            : ObservableField<String>? = null
    var departmentStr //部门
            : ObservableField<String>? = null
    var carNumStr = ObservableField("京A8888") //车牌照
    var colorStr: ObservableField<String>? = null //车辆颜色值
    var selColorId = 0 //选中车的颜色id
    var parkId = 47 //
    var carId = 0//车id
    var parkName:String = ""
    var colorData: ObservableField<ArrayList<Any>>? = null
    var imgData //保存全部拍照后的图片路径集合
            : ObservableField<ArrayList<Any>>? = null
    val imgs = ArrayList<Any>()
    val colors = ArrayList<Any>()
    var resultFlag = false //来访预约界面进入标志

    val clickListener = ClickListener()


    private val paths: ArrayList<String> = ArrayList()  //上传服务器需要用到的地址
    val colorType:Int = 1
    val imgType:Int = 2
    private val startStr = "开始时间"
    private val endStr = "结束时间"
    val listener:ApplyParkingItemListener
    init {
        listener = ApplyParkingItemListener()
        colorData = ObservableField(colors)
        //添加默认添加数据
        imgs.add("+")
        imgData = ObservableField(imgs)
        imgData?.notifyChange()

    }

    private var userEntity //用户详情
            : LoginValues? = null
    override fun createModel(): ApplyParkingModel {
        return ApplyParkingModel()
    }

    override fun initData() {
        //获得当前登陆的用户信息
        userEntity = m.requestUser(getApplication())
        //请求车辆颜色数据
        requestColor()
        //初始化单向数据绑定的数据
        initBindData()
    }

    private fun requestColor() {
        m.request().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :Observer<BaseEntity>{
                override fun onSubscribe(d: Disposable) {
                    addCompositeDisposable(d)
                    showDialog()

                }

                override fun onNext(t: BaseEntity) {
                    if(t.statuesCode == "200"){
                        val carColorEntity = t as CarColorEntity
                        colors.addAll(carColorEntity.getValues())
                        colorData?.set(colors)
                        colorData?.notifyChange()
                    }

                }

                override fun onError(e: Throwable) {
                   disDialog()
                    showMsg("请求失败")
                }

                override fun onComplete() {
                    disDialog()
                }

            })
    }

    private fun initBindData() {
        timeStr = ObservableField("$startStr-$endStr")
        applyTimeStr = ObservableField(TimeUtils.millis2String(System.currentTimeMillis(),
            SimpleDateFormat("yyyy-MM-dd")))
        nameStr = ObservableField(userEntity?.uName)
        departmentStr = ObservableField("技术部")
        colorStr = ObservableField("#cccccc")
        timeStr!!.notifyChange()
        applyTimeStr!!.notifyChange()
        nameStr!!.notifyChange()
        departmentStr!!.notifyChange()
        carNumStr.notifyChange()
        colorStr!!.notifyChange()
    }

    override fun onNext(t: BaseEntity) {

    }
    //打开相机
    private fun openCamera() {
        EventBus.getDefault().post("openCamera")
    }
    //将拍照后的图片存储到images
    fun serveImgPath(path:String){
        imgs.add(0,path)
        imgData?.set(imgs)
        imgData?.notifyChange()
    }
    inner class ClickListener{
        fun click(){
            //上传全部车辆照片到服务器
            uploadImgs()

        }
        fun selTimer(){

        }
    }

    /**
     * 上传车辆图片
     */
    private fun uploadImgs() {
        imgData?.get()?.let {
            if(it.isEmpty() || it[0] == "+" ){
                addCarInfo()
                return
            }
             it.forEach {
                 if(it != "+"){
                     paths.add(it as String)
                 }
             }
            //上传服务器
            m.uploadImages(paths).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Observer<BaseEntity>{
                    override fun onSubscribe(d: Disposable) {
                        addCompositeDisposable(d)
                        showDialog()
                    }

                    override fun onNext(t: BaseEntity) {

                    }

                    override fun onError(e: Throwable) {
                        disDialog()
                    }

                    override fun onComplete() {
                        disDialog()
                        //上传全部图片完成->添加车辆信息
                        //生成服务器图片地址
                        addCarInfo()
                    }
                })
        }
    }

    /**
     * 上传车辆信息
     */
    private fun addCarInfo() {
        val map = HashMap<String, Any?>()
        map.put("carNum",carNumStr.get())
        map.put("colorId",selColorId)
        map.put("parkId",parkId)
        val toJson = Gson().toJson(map).toString()

        val requestBody = RequestBody.create(MediaType.parse("application/json"),toJson )


        m.requestAddCar(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :Observer<BaseEntity>{
                override fun onSubscribe(d: Disposable) {
                    addCompositeDisposable(d)
                    showDialog()
                }

                override fun onNext(t: BaseEntity) {
                    //车辆id
                    val entity = t as CarEntity
                    carId = entity.getValues().carId.toInt()
                }

                override fun onError(e: Throwable) {
                    disDialog()
                }

                override fun onComplete() {
                    disDialog()
                    addCarImg()

                }
            })
    }

    /**
     * 绑定车辆和图片
     */
    private fun addCarImg() {
        if (paths.size == 0) {
            showMsg("车辆添加成功")
            addParking()
            return
        }
        //车位+图片
        val list: MutableList<Map<String, Any>> = ArrayList()
        for (str in paths) {
            val map: MutableMap<String, Any> = java.util.HashMap()
            map["carId"] = carId
            map["imgPath"] = str.substring(str.lastIndexOf("/") + 1)
            list.add(map)
        }
        val params: MutableMap<String, List<*>> = HashMap()
        params["imgs"] = list
        val requestBody = RequestBody.create(MediaType.parse("application/json"), Gson().toJson(params))
        m.requestAddCarImages(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :Observer<BaseEntity>{
                override fun onSubscribe(d: Disposable) {
                    addCompositeDisposable(d)
                    showDialog()
                }

                override fun onNext(t: BaseEntity) {

                }

                override fun onError(e: Throwable) {
                    disDialog()
                    showMsg("" + e.message)
                }

                override fun onComplete() {
                    disDialog()
                    if(!resultFlag){//不是来访预约
                        //提交车位信息到服务器
                        addParking()
                    }else{//来访预约跳转

                    }
                }
            })
    }

    /**
     * 车位申请
     */
    private fun addParking() {
        val map: MutableMap<String, Any> = HashMap()
        map["parkId"] = parkId
        map["parkUser"] = userEntity!!.uId
        map["parkName"] = parkName
        //后期日期时间选择器比较好
        map["parkStart"] = "" + System.currentTimeMillis() / 1000
        map["parkEnd"] = "" + System.currentTimeMillis() / 100
        map["parkTime"] = "" + System.currentTimeMillis() / 1000
        val requestBody = RequestBody.create(MediaType.parse("application/json"), Gson().toJson(map))
        m.requestAddCarImages(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :Observer<BaseEntity>{
                override fun onSubscribe(d: Disposable) {
                    addCompositeDisposable(d)
                    showDialog()
                }

                override fun onNext(t: BaseEntity) {
                   if(t.statuesCode == "200"){
                      val entity = t as AddSysParkingEntity
                       entity.parkingId = parkId

                   }
                }

                override fun onError(e: Throwable) {
                    showMsg("请求出错:" + e.message)
                    disDialog()
                }

                override fun onComplete() {
                    disDialog()
                    finishAct()
                }
            })

    }

    //item的点击
    inner class ApplyParkingItemListener:BaseQuickAdapter.OnItemClickListener {
        override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {


            if(adapter is ColorAdapter){
                val colorValue = adapter.data[position].colorValue
                colorStr?.set(colorValue)
            }else if(adapter is CarImgAdapter){
                if(adapter.data.size >= 7){
                    showMsg("最多只能添加6张图片")
                    return
                }
                openCamera()
            }
        }



    }
}


