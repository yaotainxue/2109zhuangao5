package com.ostrich.a2109park.mvvm.model

import com.ostrich.common.entity.BaseEntity
import com.ostrich.common.mvvm.base.BaseModel
import com.ostrich.common.network.HttpRetrofitFactory
import com.ostrich.common.network.HttpType
import io.reactivex.Observable
import io.reactivex.internal.functions.Functions
import okhttp3.RequestBody


/**
 * @Author : wjs
 * @Time : On 2024/3/26 11:01
 * @Description : LoginModel
 */
class LoginModel: BaseModel() {
    override fun cleared() {

    }

    //vararg:表示可以传0个或多个参数
    override fun request(vararg bodyes: RequestBody): Observable<BaseEntity> {
        //使用Retrofit构建对应用户登录
        //使用哪个类型的请求方式->HttpType.SIGNINTERFACE->由于当前接口只有sign签名
        //使用只添加sign拦截的建造者建造对应retrofit对象
        //转化为基类的BaseEntity->使用Observable下的flatmap操作符转换

        //Observable<LoginEntity>
        val observable = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.SIGN)
            .getRetrofit()
            .create(Api::class.java)
            .requestLogin(bodyes[0])
        //Observable<LoginEntity>-->>Observable<BaseEntity>
        return changeOB(observable)
    }
}