package com.ostrich.a2109park.mvvm.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import com.ostrich.a2109park.R
import com.ostrich.a2109park.BR
import com.ostrich.a2109park.databinding.ActivityApplyParkingBinding
import com.ostrich.a2109park.mvvm.viewmodel.ApplyParkingViewModel
import com.ostrich.a2109park.tensorflow.TensorflowActivity
import com.ostrich.common.filed.ActivityField
import com.ostrich.common.mvvm.base.BaseActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ApplyParkingActivity : BaseActivity<ActivityApplyParkingBinding,ApplyParkingViewModel>() {
    var path = ""
    override fun initLayout(): Int {
        return R.layout.activity_apply_parking
    }

    override fun initView() {
        intent?.getBundleExtra(ActivityField.ACTBUNDLE)?.let {
            vm.parkId = it.getInt(ActivityField.ACTBUNDLE)
            vm.parkName = it.getString("parkName","")
            vm.resultFlag = it.getBoolean(ActivityField.FORRESULT)
        }
        EventBus.getDefault().register(this)
        v.clicklistener = vm.clickListener
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    /**
     * 打开相机
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun openCamera(event:String){
        path = externalCacheDir!!.absolutePath+"/"+System.currentTimeMillis()+".png"
        val intent = Intent(this,TensorflowActivity::class.java)
        intent.putExtra("path",path)
        startActivityForResult(intent,200)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode != RESULT_OK){
            return
        }
        //将图片路径发送到viewmodel中
        if(requestCode == 200) {
            vm.serveImgPath(path)
        }
    }

    override fun initVariable(): Int {
     return BR.vm
    }

}