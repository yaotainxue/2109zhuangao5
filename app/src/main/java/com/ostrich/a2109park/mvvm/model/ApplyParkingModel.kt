package com.ostrich.a2109park.mvvm.model

import android.app.Application
import com.ostrich.a2109park.dao.LoginUserDao
import com.ostrich.a2109park.mvvm.model.entity.LoginValues
import com.ostrich.common.entity.BaseEntity
import com.ostrich.common.mvvm.base.BaseModel
import com.ostrich.common.network.HttpRetrofitFactory
import com.ostrich.common.network.HttpType
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart
import java.io.File


/**
 * @Author : wjs
 * @Time : On 2024/3/26 11:01
 * @Description : LoginModel
 */
class ApplyParkingModel: BaseModel() {
    private var serve //获取用户本地缓存数据
            : LoginUserDao? = null
    override fun cleared() {

    }

    /**
     * 数据库查询用户当前信息
     */
    fun requestUser(application: Application):LoginValues{
        serve = LoginUserDao.getInstance(application)
        return serve?.selUser()!!
    }

    /**
     * 查询车辆颜色
     */
    override fun request(vararg bodyes: RequestBody): Observable<BaseEntity> {
        val observable = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.TOKEN)
            .getRetrofit()
            .create(Api::class.java)
            .requestCarColor()
        //Observable<LoginEntity>-->>Observable<BaseEntity>
        return changeOB(observable)
    }
    /**
     * 上传图片到服务器：多图上传 图片地址-----》MultipartBody.Part
     */
    fun uploadImages(data:List<String>):Observable<BaseEntity>{
        val list = ArrayList<MultipartBody.Part>()
        data.forEach {
            val requestBody = RequestBody.create(MediaType.parse("image/png"), File(it))
            val multipart = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", it.substring(it.lastIndexOf("/")), requestBody)
                .build()
            list.add(multipart.part(0))
        }
        val uploadIms = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.UPLOAD)
            .getRetrofit()
            .create(Api::class.java)
            .uploadIms(list)
        return changeOB(uploadIms)
    }

    /**
     * 上传车信息
     */
    fun requestAddCar(vararg bodys: RequestBody):Observable<BaseEntity> {
        val requestAddCar = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.TOKEN)
            .getRetrofit()
            .create(Api::class.java)
            .requestAddCar(bodys[0])
        return changeOB(requestAddCar)
    }

    /**
     * 车+图片
     */
    fun requestAddCarImages(vararg bodys: RequestBody):Observable<BaseEntity> {
        val requestAddCar = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.TOKEN)
            .getRetrofit()
            .create(Api::class.java)
            .requestCarImages(bodys[0])
        return changeOB(requestAddCar)
    }
    /**
     * 车位申请
     */
    fun requestAddSysParking(vararg bodys: RequestBody):Observable<BaseEntity> {
        val requestAddCar = HttpRetrofitFactory.getInstance()
            .createHttpRetrofit(HttpType.TOKEN)
            .getRetrofit()
            .create(Api::class.java)
            .requestAddSysParking(bodys[0])
        return changeOB(requestAddCar)
    }

}