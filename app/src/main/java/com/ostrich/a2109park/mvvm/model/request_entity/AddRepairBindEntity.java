package com.ostrich.a2109park.mvvm.model.request_entity;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;

/**
 * 添加维修申请界面databinding需要关联的数据源封装
 * 界面:
 * 1.申请日期;2.申请人;3.所属部门;4.维修日期;5.问题描述
 * 6.维修图片;
 * 请求相关数据:
 * {
 "departmentId": 0,
 "repairData": "string",
 "repairDescribe": "string",
 "repairId": 0,
 "repairTime": "string",
 "repairUserId": 0,
 "userId": 0
 }
 * */
public class AddRepairBindEntity extends BaseObservable{

    //请求相关字段数据
    public int dept_Id;
    public int repairId;
    public int repairUserId;

    //xml中关联的数据
    private String appointData;
    private String userName;
    private String departmentName;
    private String repairData;
    private String describe;
    private List<Object>imges;

    public String getAppointData() {
        return appointData;
    }

    public void setAppointData(String appointData) {
        this.appointData = appointData;
        notifyChange();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        notifyChange();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
        notifyChange();
    }

    public String getRepairData() {
        return repairData;
    }

    public void setRepairData(String repairData) {
        this.repairData = repairData;
        notifyChange();
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
        notifyChange();
    }

    public List<Object> getImges() {
        return imges;
    }

    public void setImges(List<Object> imges) {
        this.imges = new ArrayList<>(imges);
        notifyChange();
    }
}
