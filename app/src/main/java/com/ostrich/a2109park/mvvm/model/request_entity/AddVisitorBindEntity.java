package com.ostrich.a2109park.mvvm.model.request_entity;

import androidx.databinding.BaseObservable;

/**
 * 统一管理界面中需要databinding单向绑定的数据源
 * {
 "carNum": "string",
 "parkId": 0,
 "userId": 0,
 "visitorDescribe": "string",
 "visitorEnd": "string",
 "visitorStart": "string",
 "visitorTime": "string"
 }
 * */
public class AddVisitorBindEntity extends BaseObservable{

    //发起网络请求时使用数据源
    public int parkId;//车位id
    public int userId;//使用车位的用户id
    public String endTime = ""+(System.currentTimeMillis()/1000);
    public String startTime = ""+(System.currentTimeMillis()/1000);

    //databinding下需要映射的数据源
    private String data;//来访日期
    private String time;//来访时长
    private String UserName;//申请人
    private String department;//所属部门
    private String nowTime = ""+(System.currentTimeMillis()/1000);//申请日期
    private boolean isCar;//是否驾车来访
    private String carNum;//车牌号码
    private String partName;//车位名称
    private String describe = "hehe";//描述

    //统一添加get，set方法
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
        notifyChange();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        notifyChange();
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
        notifyChange();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
        notifyChange();
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
        notifyChange();
    }

    public boolean isCar() {
        return isCar;
    }

    public void setCar(boolean car) {
        isCar = car;
        notifyChange();
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
        notifyChange();
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
        notifyChange();
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
        notifyChange();
    }


}
