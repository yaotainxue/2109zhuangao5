package com.ostrich.a2109park.mvvm.viewmodel

import android.app.Application
import android.os.Bundle
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.SPUtils
import com.ostrich.a2109park.mvc.MainActivity
import com.ostrich.a2109park.dao.LoginUserDao
import com.ostrich.a2109park.mvvm.model.LoginModel
import com.ostrich.a2109park.mvvm.model.entity.LoginEntity
import com.ostrich.a2109park.mvvm.model.request_entity.UserEntity
import com.ostrich.common.entity.BaseEntity
import com.ostrich.common.filed.ActivityField
import com.ostrich.common.mvvm.base.BaseOBViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/26 11:01
 * @Description : LoginViewModel
 */
class LoginViewModel(application: Application) :BaseOBViewModel<LoginModel>(application){
    var listener:LoginListener? = null
        get(){
           return LoginListener()
        }
    var entity: UserEntity?=null
    lateinit var dao:LoginUserDao //  lateinit var 延迟初始化
    init {
        entity = UserEntity("zxy","123456")
        dao =  LoginUserDao.getInstance(getApplication())
    }
    //databinding下绑定用户名以及密码数据
    //由于当前viewmodel层只负责向xml中设置数据
    //无获取方法调用权限,所以需要databinding数据
    //双向绑定方式进行绑定,从而获取用户输入的数据
    override fun createModel(): LoginModel {
        return LoginModel()
    }

    override fun initData() {

    }

    override fun onNext(t: BaseEntity) {
        //处理登陆请求结果  kotlin中==和java中equals       kotlin中===和java中==
        if(t.statuesCode != "200"){
            showMsg("onNext:请求出错:" + t.msg)
            return
        }
        //向下转型为对应的LoginUserEntity->请求返回的实体类
        val loginEntity =  t as LoginEntity //as强转
        //保存token到sp存储中
        SPUtils.getInstance().put("token",loginEntity.getValues().token)
        //保存homeurl->主页网址->记住用户登录状态时；从启屏页跳转到框架页时
        //框架页需要显示h5页面
        SPUtils.getInstance().put("home",loginEntity.home)
        LogUtils.e("home:" + loginEntity.home)
        //保存用户信息到本地sqlite
        val insert = dao.insert(loginEntity.getValues())
        if(insert <= 0){
            showMsg("用户本地保存失败")
        }
        //跳转到软件框架页用来显示首页的h5
        val bundle = Bundle()
        bundle.putString(ActivityField.ACTBUNDLE, loginEntity.home)
        val map: MutableMap<String, Any> = HashMap()
        map[ActivityField.ACTCLAZZ] = MainActivity::class.java
        map[ActivityField.ACTBUNDLE] = bundle
        startAct(map)
        finishAct()


    }

    //内部类必须添加inner
    inner class LoginListener{
        fun loginClick(){

            //发起登陆请求
            //封装请求的JSON
            val map = mutableMapOf<String,String>()
            entity?.userName?.let {
                map.put("uName", it)
            }
            entity?.userPwd?.let {
                map.put("uPwd", it)
            }
            map["time"] = (System.currentTimeMillis() / 1000).toString()
            //封装请求体
            val body: RequestBody =
                RequestBody.create(MediaType.parse("application/json"), map.toString())
            //发起请求
            m.request(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( this@LoginViewModel)
        }
    }
}