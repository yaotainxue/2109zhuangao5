package com.ostrich.a2109park.mvvm.model

import com.ostrich.a2109park.mvvm.model.entity.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * @Author : wjs
 * @Time : On 2024/3/28 08:15
 * @Description : Api
 */
interface Api {
    //用户登录接口
    @POST("sysUser/loginUser")
    fun requestLogin(@Body body: RequestBody): Observable<LoginEntity>

    //全部车位接口
    @POST("sysParking/selParking")
    fun requestParkingAll(): Observable<ParkingAllEntity>

    //获取车辆颜色
    @POST("sysColor/selColor")
    fun requestCarColor(): Observable<CarColorEntity>

    //全部图片上传
    @Multipart
    @POST("fileUpload")
    fun uploadIms(@Part list: List<MultipartBody.Part>): Observable<UploadImagesEntity>
    //上传车辆信息
    @POST("sysCar/addCar")
    fun requestAddCar(@Body body: RequestBody): Observable<CarEntity>
    //上传车辆+图片信息
    @POST("sysCar/addCarImg")
    fun requestCarImages(@Body body: RequestBody): Observable<CarImgEntity>

    //申请车位
    @POST("sysParking/applyParking")
    fun requestAddSysParking(@Body body: RequestBody): Observable<AddSysParkingEntity>

}