package com.ostrich.a2109park.mvvm.model.entity

import com.google.gson.Gson
import com.ostrich.common.entity.BaseEntity
import org.json.JSONArray
import org.json.JSONException

/**
 * @Author : wjs
 * @Time : On 2024/4/1 09:57
 * @Description : CarColorEntity
 */
data class CarColorEntity( val values: String,
                           var list: MutableList<CarColorValues> ,
): BaseEntity(){
    fun getValues(): List<CarColorValues> {
        if (list == null) {
            list = mutableListOf()
            if (values == "[]") return list
            try {
                val jay = JSONArray(values)
                for (i in 0 until jay.length()) {
                    val v: CarColorValues =
                        Gson().fromJson(jay.getJSONObject(i).toString(), CarColorValues::class.java)
                    list.add(v)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return list
    }
}
data class CarColorValues(
    val colorId: Int = 0,
    val colorValue: String = "#000000"
)
