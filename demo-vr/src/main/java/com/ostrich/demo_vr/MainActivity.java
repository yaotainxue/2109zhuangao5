package com.ostrich.demo_vr;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private VrPanoramaView paNormalView;
    private VrPanoramaView.Options paNormalOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initVrPaNormalView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        paNormalView.pauseRendering();
    }

    @Override
    protected void onResume() {
        super.onResume();
        paNormalView.resumeRendering();
    }

    @Override
    protected void onDestroy() {
        // Destroy the widget and free memory.
        super.onDestroy();
        paNormalView.shutdown();
    }


    //初始化VR图片
    private void initVrPaNormalView() {
        paNormalView = (VrPanoramaView) findViewById(R.id.vrPanoramaView);
        paNormalOptions = new VrPanoramaView.Options();
        paNormalOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
//        paNormalView.setFullscreenButtonEnabled (false); //隐藏全屏模式按钮
        paNormalView.setInfoButtonEnabled(false); //设置隐藏最左边信息的按钮
        paNormalView.setStereoModeButtonEnabled(false); //设置隐藏立体模型的按钮
        paNormalView.setEventListener(new ActivityEventListener()); //设置监听
        //加载本地的图片源
        paNormalView.loadImageFromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.cc), paNormalOptions);
        //设置网络图片源
//        panoWidgetView.loadImageFromByteArray();
    }

    private class ActivityEventListener extends VrPanoramaEventListener {
        @Override
        public void onLoadSuccess() {//图片加载成功
            Log.i(TAG, "onLoadSuccess------------>");
        }


        @Override
        public void onLoadError(String errorMessage) {//图片加载失败
            Log.i(TAG, "Error loading pano:------------> " + errorMessage);
        }

        @Override
        public void onClick() {//当我们点击了VrPanoramaView 时候出发
            super.onClick();
            Log.i(TAG, "onClick------------>");
        }

        @Override
        public void onDisplayModeChanged(int newDisplayMode) {
            //改变显示模式时候出发（全屏模式和纸板模式）
            super.onDisplayModeChanged(newDisplayMode);
            Log.i(TAG, "onDisplayModeChanged------------>" + newDisplayMode);
        }
    }
}