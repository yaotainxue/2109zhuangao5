package com.ostrich.demo_builder;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 测试代码
        Student student = new Student.Builder()
                .setPhone("18630704579")
                .setName("zhangsan")
                .build();

        student.show();
    }
}