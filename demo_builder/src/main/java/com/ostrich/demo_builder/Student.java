package com.ostrich.demo_builder;

import android.util.Log;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 09:10
 * @Description : Student
 */
public class Student {
    private String name;
    private int age;
    private String sex;
    private String address;
    private String phone;
    private String email;
    private String id;
    private String grade;
    private String major;
    private String school;
    private String college;
    private String teacher;
    private String teacherPhone;

    public void show(){
        Log.d("Student", "show: "+name);
    }

    public Student(Builder builder) {
        this.address = builder.address;
        this.phone = builder.phone;
        this.email = builder.email;
        this.id = builder.id;
        this.grade = builder.grade;
        this.major = builder.major;
        this.school = builder.school;
        this.college = builder.college;
        this.teacher = builder.teacher;
        this.teacherPhone = builder.teacherPhone;
        this.name = builder.name;
        this.age = builder.age;
        this.sex = builder.sex;
        this.teacherPhone = builder.teacherPhone;
    }


    //TODO 1:Builder
    public static class Builder {
        private String name;
        private int age;
        private String sex;
        private String address;
        private String phone;
        private String email;
        private String id;
        private String grade;
        private String major;
        private String school;
        private String college;
        private String teacher;
        private String teacherPhone;


        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setSex(String sex) {
            this.sex = sex;
            return this;
        }
        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }
        public Builder setGrade(String grade) {
            this.address = address;
            return this;
        }
        public Builder setId(String id) {
            this.id = id;
            return this;
        }
        public Builder setTeacher(String teacher) {
            this.teacher = teacher;
            return this;
        }
        public Builder setCollege(String college) {
            this.college = college;
            return this;
        }
        public Builder setTeacherPhone(String teacherPhone) {
            this.teacherPhone = teacherPhone;
            return this;
        }


        public Student build(){
            return new Student(this);
        }





    }


}
