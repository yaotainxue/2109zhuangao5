package com.ostrich.demo_factory;

import android.util.Log;

import androidx.annotation.LongDef;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 09:59
 * @Description : ProductA
 */
public class ProductA implements Product{
    @Override
    public void produce() {
        Log.d("TAG", "produceA: ");
    }
}
