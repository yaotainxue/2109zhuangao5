package com.ostrich.demo_factory;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 10:01
 * @Description : ProductFactory 懒汉双重校验锁单例
 */
public class ProductFactory {
    private ProductFactory(){}
    private volatile static ProductFactory instance;//volatile保证有序和可见
    public static  ProductFactory getInstance(){
        if(instance==null){//第一层：提高性能，不需要每次都执行锁
            synchronized (ProductFactory.class){ //synchronized保证同一时刻只有一个线程执行
                if(instance==null){//第二层：保证线程安全，只有一个实例
                    instance=new ProductFactory();
                }
            }
        }
        return instance;
    }
    private ProductA mProductA;
    private ProductB mProductB;
    private ProductC mProductC;

    /**
     * 创建产品
     * @param type
     * @return
     */
    public Product createProduct(ProductType type){
        switch (type){
            case A:
                if(mProductA == null){
                    mProductA = new ProductA();
                }
                return mProductA;
            case B:
                if(mProductB == null){
                    mProductB = new ProductB();
                }
                return mProductB;
            case C:
                if(mProductC == null){
                    mProductC = new ProductC();
                }
                return mProductC;
            default:
                return null;
        }

    }



}
