package com.ostrich.demo_factory;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 10:01
 * @Description : ProductType 枚举
 */
public enum ProductType {
    A,B,C
}
