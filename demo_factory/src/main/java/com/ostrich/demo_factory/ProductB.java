package com.ostrich.demo_factory;

import android.util.Log;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 09:59
 * @Description : ProductB
 */
public class ProductB implements Product{
    @Override
    public void produce() {
        Log.d("TAG", "ProductB: ");
    }
}
