package com.ostrich.demo_factory;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * 工厂模式：封装对象的创建过程，使代码更加灵活和可维护
 * 1.接口：产品
 * 2.实现子类：产品ABC
 * 3.枚举不同的类型
 * 4.工厂类：创建产品  单例模式
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProductFactory.getInstance().createProduct(ProductType.A).produce();

    }
}