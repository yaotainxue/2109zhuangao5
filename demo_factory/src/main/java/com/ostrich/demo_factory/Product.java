package com.ostrich.demo_factory;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/23 09:58
 * @Description : Product 产品接口
 */
public interface Product {
    void produce();
}
