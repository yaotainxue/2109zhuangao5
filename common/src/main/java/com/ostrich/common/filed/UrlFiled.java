package com.ostrich.common.filed;
/**
 * 专门管理网络请求接口地址公共部分
 * 存放网络请求常量
 * */
public final class UrlFiled {

    //生产环境
    public static final String BASEURL = "http://10.161.9.80:7035/";
    public static final String IMGURL = "http://10.161.9.80:7035/fileDownload?fileName=";
    //测试环境
//    public static final String BASEURL = "http://192.168.31.107:8080/";
//    public static final String IMGURL = "http://192.168.31.107:8080/fileDownload?fileName=";
    //拼接到每次网络请求的参数中
//    public static final String TOKENKEY = "token";//是否需要token常量->出门卡->服务器提供且需要保存到本地
//    public static final String SIGNKEY = "sign";//是否需要sign常量
//    public static final int KEYNO = 0;//不使用token或者sign
//    public static final int KEYOK = 1;//使用token或者sign
}
