package com.ostrich.common.filed;
/**
 * activity相关操作中的常量意图
 * */
public final class ActivityField {

    public static final String ACTCLAZZ = "CLAZZACTION";//要跳转的activity类型
    public static final String ACTBUNDLE = "BUNDLEACTION";//bundler传递的参数
    public static final String FORRESULT = "FORRESULT";//带返回值方式启动activity

}
