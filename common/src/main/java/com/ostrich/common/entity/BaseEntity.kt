package com.ostrich.common.entity

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/22 09:24
 * @Description : BaseEntity
 * 全部网络请求的基类
 * 根据网络请求接口公共部分提取
 */
open class BaseEntity(val statuesCode:String = "",val msg:String = "")
