package com.ostrich.common.mvvm.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.ostrich.common.filed.ActivityField;
import com.ostrich.common.mvvm.IView;
import com.ostrich.common.widget.LoadDialog;

import java.lang.reflect.ParameterizedType;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


/**
 * View层中全部fragment基类
 * */
public abstract class BaseFragment<V extends ViewDataBinding,VM extends BaseViewModel> extends Fragment implements IView {

    protected V v;//与当前fragment相关联的Databinding
    protected VM vm;//与当前fragment相关联的viewmodel
    private Observer<String>showDialogOB;//vm下的显示dialog事件
    private Observer<String>disDialogOB;//vm下的关闭dialog事件
    private Observer<String>finishOB;//vm下的关闭界面事件
    private Observer<Map<String,Object>>startOB;//vm下的启动activity事件
    private LoadDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        BaseApplication.getInstance().refWatcher.watch(this);
        initVM();
        initView();
        createLiveDataOB();
        registerUIChangedListener();
        getLifecycle().addObserver(vm);
        return v.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (v!=null)
            v.unbind();
        vm.getUiChangedListner().getShowDialogData().removeObserver(showDialogOB);
        vm.getUiChangedListner().getDisDialogData().removeObserver(disDialogOB);
        vm.getUiChangedListner().getFinishActData().removeObserver(finishOB);
        vm.getUiChangedListner().getStartActData().removeObserver(startOB);
        getLifecycle().removeObserver(vm);
    }

    @Override
    public void initVM() {
        v = DataBindingUtil.inflate(LayoutInflater.from(getContext()),initLayout(),null,false);
        Class clz;
        ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
        clz = (Class) type.getActualTypeArguments()[1];
        vm = createViewModel(clz);
        v.setVariable(initVariable(),vm);
    }

    //创建viewModel对象方法
    private <T extends BaseViewModel>T createViewModel(Class clz){
        return (T) new ViewModelProvider(this).get(clz);
    }

    //显示dialog
    private void showDialog(){
        dialog = new LoadDialog(getContext());
        dialog.show();

    }
    //隐藏dialog
    private void disDialog(){
        if (dialog!=null)
            dialog.dismiss();
    }
    //关闭界面
    private void finishACT(){
        getActivity().finish();
    }
    //跳转界面
    private void startAct(Map<String,Object>map){
        Class clz = (Class) map.get(ActivityField.ACTCLAZZ);
        Bundle bundle = (Bundle) map.get(ActivityField.ACTBUNDLE);
        Intent intent = new Intent(getActivity(),clz);
        if (bundle!=null){
            intent.putExtra(ActivityField.ACTBUNDLE,bundle);
        }
        startActivity(intent);
    }

    //创建viewmodle下的livedata观察者对象
    private void createLiveDataOB(){
        showDialogOB = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                showDialog();
            }
        };

        disDialogOB = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                disDialog();
            }
        };

        finishOB = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                finishACT();
            }
        };

        startOB = new Observer<Map<String, Object>>() {
            @Override
            public void onChanged(@Nullable Map<String, Object> map) {
                startAct(map);
            }
        };
    }
    //注册viewmodel下的livedata观察者
    private void registerUIChangedListener(){
        vm.getUiChangedListner().getShowDialogData().observeForever(showDialogOB);
        vm.getUiChangedListner().getDisDialogData().observeForever(disDialogOB);
        vm.getUiChangedListner().getFinishActData().observeForever(finishOB);
        vm.getUiChangedListner().getStartActData().observeForever(startOB);
    }

}
