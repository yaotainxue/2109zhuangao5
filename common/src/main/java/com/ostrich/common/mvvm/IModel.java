package com.ostrich.common.mvvm;
/**
 * model层顶层接口封装
 * 抽离出cleared方法
 * 在生命周期结束时
 * 断开对应请求
 * */
public interface IModel {

    void cleared();
}
