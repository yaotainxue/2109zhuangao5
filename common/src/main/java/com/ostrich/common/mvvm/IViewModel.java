package com.ostrich.common.mvvm;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * ViewModel层顶层接口
 * 继承lifecycle->绑定activity/fragment生命周期
 * */
public interface IViewModel extends LifecycleObserver {
    //绑定oncreate方法->创建
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate();
    //绑定的destroy方法->销毁
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy();

}
