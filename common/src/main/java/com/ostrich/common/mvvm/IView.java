package com.ostrich.common.mvvm;
/**
 * View层，接口
 * 封装载入xml方法
 * 封装初始化viewmodel方法
 * 初始化控件
 * databinding绑定viewmodel时的类型方法
 * */
public interface IView {

    //要绑定的xmlid
    int initLayout();

    //初始化控件方法
    void initView();

    //初始化viewmodel方法
    void initVM();

    //返回databinding类型方法
    int initVariable();

}
