package com.ostrich.common.mvvm.base;

import android.content.Intent;
import android.os.Bundle;

import com.blankj.utilcode.util.LogUtils;
import com.jaeger.library.StatusBarUtil;
import com.ostrich.common.R;
import com.ostrich.common.filed.ActivityField;
import com.ostrich.common.mvvm.IView;
import com.ostrich.common.widget.LoadDialog;


import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


public abstract class BaseActivity<V extends ViewDataBinding,VM extends BaseViewModel> extends AppCompatActivity implements IView {

    protected V v;//与activity关联的databinding
    protected VM vm;//与activity绑定的viewmodel
    private Observer<String>showDialogOb;//显示dialog事件的livedata观察者
    private Observer<String>disDialogOb;//隐藏dialog事件的livedata观察者
    private Observer<String>finishOb;//关闭界面事件的livedata观察者
    private Observer<Map<String,Object>>startOb;//activity启动事件的livedata观察者
    private ArrayList<BaseLiveData>observableList=new ArrayList<>();
    private ArrayList<Observer>observerList=new ArrayList<>();
    private LoadDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        BaseApplication.getInstance().refWatcher.watch(this);
        initVM();
        //导入沉浸式状态栏颜色
        StatusBarUtil.setColor(this,getResources().getColor(R.color.statuescolor),0);
        //创建viewmodel下使用的livedata观察者
        createLiveDataOB();
        //Viewmodel中livedata数据事件注册
        registerUIChanged();
        initView();
        //添加viewmodel绑定
        getLifecycle().addObserver(vm);
    }

    protected void bindLiveData(BaseLiveData liveData, Observer observer){
        if (observableList.remove(liveData)&&observerList.remove(observer)){
            liveData.removeObserver(observer);
        }
        observableList.add(liveData);
        observerList.add(observer);
        liveData.observeForever(observer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //解除databinding绑定
        if (v!=null)
            v.unbind();
        if (observerList.size() > 0){
            for (int i = 0;i < observerList.size();i ++){
                observableList.get(i).removeObserver(observerList.get(i));
            }
            observableList.clear();
            observerList.clear();
        }
        //解除viewmodel下的livedata的观察者
        vm.getUiChangedListner().getShowDialogData().removeObserver(showDialogOb);
        vm.getUiChangedListner().getDisDialogData().removeObserver(disDialogOb);
        vm.getUiChangedListner().getFinishActData().removeObserver(finishOb);
        vm.getUiChangedListner().getStartActData().removeObserver(startOb);
        //解除viewmodel与activity间的lifecycle的绑定
        getLifecycle().removeObserver(vm);
    }

    //初始化viewmodel以及Databinding
    //将viewmodel绑定到databing中去
    //响应式编程
    @Override
    public void initVM() {
        //获取databinding对象
        v = DataBindingUtil.setContentView(this,initLayout());
        //通过反射获取当前类的泛型(viewmodel)类型
        Class clz;
        ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
        clz = (Class) type.getActualTypeArguments()[1];
        vm = createViewModel(clz);
        vm.setCtx(this);
        //将viewmodel设置到databinding中
        v.setVariable(initVariable(),vm);//initVeriable()绑定到databinding中的类型,vm对应的viewmodel

    }

    //构建viewmodel对象方法
    //Class clz->ViewModel类型
    private <T extends BaseViewModel>T createViewModel(Class clz){

        return (T) new ViewModelProvider(this).get(clz);

    }
    //注册BaseViewModel下的livedata观察者方法
    private void registerUIChanged(){
        vm.getUiChangedListner().getShowDialogData().observeForever(showDialogOb);
        vm.getUiChangedListner().getDisDialogData().observeForever(disDialogOb);
        vm.getUiChangedListner().getFinishActData().observeForever(finishOb);
        vm.getUiChangedListner().getStartActData().observeForever(startOb);
    }

    //构建viewmodel下的livedata观察者方法
    private void createLiveDataOB(){
        showDialogOb = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                showDiaLog();
            }
        };
        disDialogOb = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                disDialog();
            }
        };

        finishOb = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                finishAct();
            }
        };

        startOb = new Observer<Map<String, Object>>() {
            @Override
            public void onChanged(@Nullable Map<String, Object> map) {
                startAct(map);
            }
        };
    }

    //显示dialog方法
    private void showDiaLog(){
        LogUtils.e("showDiaLog");
        dialog = new LoadDialog(this);
        dialog.show();
    }
    //关闭dialog方法
    private void disDialog(){
        LogUtils.e("disDialog");
        if (dialog!=null)
            dialog.dismiss();
    }
    //关闭界面方法
    private void finishAct(){
        finish();
    }
    //跳转界面
    private void startAct(Map<String,Object>map){
        Class clz = (Class) map.get(ActivityField.ACTCLAZZ);
        Bundle bundle = (Bundle) map.get(ActivityField.ACTBUNDLE);
        boolean resultFlag = false;
        if (map.get(ActivityField.FORRESULT)!=null)
            resultFlag = (boolean) map.get(ActivityField.FORRESULT);
        Intent intent = new Intent(this,clz);
        if (bundle!=null)
            intent.putExtra(ActivityField.ACTBUNDLE,bundle);
        if (!resultFlag)
            startActivity(intent);//标准启动
        else
            startActivityForResult(intent,100);//返回值方式启动
    }

}
