package com.ostrich.common.mvvm.base;


import com.ostrich.common.entity.BaseEntity;
import com.ostrich.common.mvvm.IModel;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.Functions;
import okhttp3.RequestBody;

public abstract class BaseModel implements IModel {

    //RequestBody... bodyes->可变参数->数组->不规定参数的数量
    //使用时可根据接口发起请求数量动态添加请求的body
    public abstract Observable<BaseEntity> request(RequestBody... bodyes);//请求服务器数据抽象方法

    //全部被观察者rxjava异步任务转化->flatmap
    protected Observable<BaseEntity> changeOB(Observable observable){
         return Observable.fromArray(observable).flatMap((Function)
                 Functions.identity(), false,
                 1, 1);
    }

}
