package com.ostrich.common.mvvm.base;

import android.app.Application;


import com.ostrich.common.entity.BaseEntity;

import androidx.annotation.NonNull;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public abstract class BaseOBViewModel<T extends BaseModel> extends BaseViewModel<T> implements
        Observer<BaseEntity> {

    public BaseOBViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onSubscribe(Disposable d) {
        addCompositeDisposable(d);
        showDialog();
    }

    @Override
    public void onError(Throwable e) {
        showMsg("请求出错:"+e.getMessage());
    }

    @Override
    public void onComplete() {
       disDialog();
    }
}
