package com.ostrich.common.mvvm.base;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.blankj.utilcode.util.StringUtils;
import com.ostrich.common.mvvm.IViewModel;


import java.util.Map;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * ViewModel层基类
 * */
public abstract class BaseViewModel<M extends BaseModel>  extends AndroidViewModel implements IViewModel {

    protected M m;//继承自BaseModel负责发起网络请求的m引用
    protected Context ctx;
    private CompositeDisposable compositeDisposable;
    private UIChangedListner uiChangedListner;

    public BaseViewModel(@NonNull Application application) {
        super(application);
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onCreate() {
        m = createModel();//创建model引用对象
        initData();//获取数据
    }

    @Override
    public void onDestroy() {
        if (compositeDisposable !=null) {
            compositeDisposable.dispose();
            compositeDisposable.clear();
        }
        if (m!=null)
            m.cleared();
    }

    //获取viewmodel中数据事件驱动器
    public UIChangedListner getUiChangedListner() {
        if (uiChangedListner == null)
            uiChangedListner = new UIChangedListner();
        return uiChangedListner;
    }

    //构建model对象
    protected abstract M createModel();

    //初始化请求服务器数据方法
    protected abstract void initData();

    //使用rxjava下的CompositeDisposable 记录绑定的异步事件每一个
    protected void addCompositeDisposable(Disposable disposable){
        if (compositeDisposable == null)
            compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(disposable);
    }

    //显示toast方法
    protected void showMsg(String msg){
        msg = StringUtils.null2Length0(msg);//utilcode下方法->当字符串msg==null时转化为长度为0的字符串""
        Toast.makeText(getApplication(),msg,Toast.LENGTH_SHORT).show();
    }

    //显示dialog方法
    protected void showDialog(){
        uiChangedListner.getShowDialogData().setValue(
                ""+System.currentTimeMillis());
        //""+System.currentTimeMillis()->当前时间戳的值作为livedata数据
        //驱动，保证livedata下的数据值每一次都发生改变->保证activity下
        //观察者每一次都会因数据改变而监听到对应事件
    }
    //关闭dialog方法
    protected void disDialog(){
        uiChangedListner.getDisDialogData().setValue(""+
                System.currentTimeMillis());
    }

    //关闭界面方法getDisDialogData()->关闭页面无效原因:
    //基类封装完成后->关闭页面使用的是关闭dialog数据驱动
    protected void finishAct(){
        uiChangedListner.getFinishActData().setValue(""+
                System.currentTimeMillis());
    }

    //界面跳转方法
    protected void startAct(Map<String,Object> map){
        uiChangedListner.getStartActData().setValue(map);
    }

    //内部类基于LiveData的数据事件驱动内部类;
    //livedata观察者模式->根据数据改变驱动activity下的事件方法进行变化
    public class UIChangedListner{
        //显示对话框事件
        BaseLiveData<String> showDialogData;
        //隐藏对话框事件
        BaseLiveData<String> disDialogData;
        //开启activity事件
        BaseLiveData<Map<String,Object>> startActData;
        //关闭activity事件
        BaseLiveData<String> finishActData;

        public BaseLiveData<String> getShowDialogData() {
            if (showDialogData == null)
                showDialogData = new BaseLiveData();
            return showDialogData;
        }

        public BaseLiveData<String> getDisDialogData() {
            if (disDialogData == null)
                disDialogData = new BaseLiveData();
            return disDialogData;
        }

        public BaseLiveData<Map<String, Object>> getStartActData() {
            if (startActData == null)
                startActData = new BaseLiveData();
            return startActData;
        }

        public BaseLiveData<String> getFinishActData() {
            if (finishActData == null)
                finishActData = new BaseLiveData();
            return finishActData;
        }
    }

}
