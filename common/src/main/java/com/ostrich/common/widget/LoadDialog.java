package com.ostrich.common.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.blankj.utilcode.util.ConvertUtils;
import com.ostrich.common.R;

import androidx.annotation.NonNull;

public class LoadDialog extends Dialog {

    private RotateAnimation rotateAnimation;
    private RelativeLayout rootView;
    private RelativeLayout contentView;
    private ImageView img;

    public LoadDialog(@NonNull Context context) {
        super(context, R.style.LoadDialog);
        init();
    }

    private void init(){
        rotateAnimation = (RotateAnimation) AnimationUtils.loadAnimation(getContext(), R.anim.load_anim);
        rotateAnimation.reset();
        rootView = new RelativeLayout(getContext());
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        rootView.setLayoutParams(lp);
        contentView = new RelativeLayout(getContext());
        RelativeLayout.LayoutParams contentLp = new RelativeLayout.LayoutParams(
                ConvertUtils.dp2px(200),
                ConvertUtils.dp2px(100));
        contentLp.addRule(RelativeLayout.CENTER_VERTICAL);
        contentView.setLayoutParams(contentLp);
//        contentView.setBackgroundResource(R.drawable.dialog_bg);
        rootView.addView(contentView);
        img = new ImageView(getContext());
        RelativeLayout.LayoutParams imgLp = new RelativeLayout.LayoutParams(
                ConvertUtils.dp2px(80),
                ConvertUtils.dp2px(80));
        imgLp.addRule(RelativeLayout.CENTER_IN_PARENT);
        img.setLayoutParams(imgLp);
        contentView.addView(img);
        img.setImageResource(R.drawable.res_icon_240);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(rootView);
    }

    @Override
    public void show() {
        super.show();
        img.startAnimation(rotateAnimation);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        rotateAnimation.cancel();
        img.clearAnimation();
    }
}
