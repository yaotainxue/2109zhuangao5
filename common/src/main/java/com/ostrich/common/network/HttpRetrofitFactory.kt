package com.ostrich.common.network

import android.content.Context

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:40
 * @Description : HttpRetrofitFactory
 */
class HttpRetrofitFactory {
    //伴生对象：单例
    companion object {
        @Volatile
        private var instance: HttpRetrofitFactory? = null

        @JvmStatic
        fun getInstance(): HttpRetrofitFactory {
            return instance ?: synchronized(this) {
                instance ?: HttpRetrofitFactory().also { instance = it }
            }
        }
    }

    private var signInterceptorRetrofitImpl:SignInterceptorRetrofitImpl? = null
    private var tokenInterceptorRetrofitImpl:TokenInterceptorRetrofitImpl? = null
    private var uploadInterceptorRetrofitImpl:UploadInterceptorRetrofitImpl? = null
    private var allInterceptorRetrofitImpl:AllInterceptorRetrofitImpl? = null

    fun createHttpRetrofit(httpType:HttpType):HttpRetrofit {




        return when (httpType) {
            HttpType.SIGN -> signInterceptorRetrofitImpl
                ?:SignInterceptorRetrofitImpl.SignSignInterceptorHttpRetrofitBuilder().build().also { signInterceptorRetrofitImpl = it }
            HttpType.TOKEN -> tokenInterceptorRetrofitImpl
                ?: TokenInterceptorRetrofitImpl.TokenInterceptorHttpRetrofitBuilder().build().also { tokenInterceptorRetrofitImpl = it }
            HttpType.UPLOAD -> uploadInterceptorRetrofitImpl
                ?: UploadInterceptorRetrofitImpl.UploadInterceptorHttpRetrofitBuilder().build().also { uploadInterceptorRetrofitImpl = it }
            HttpType.ALL -> allInterceptorRetrofitImpl
                ?: AllInterceptorRetrofitImpl.AllInterceptorHttpRetrofitBuilder().build().also { allInterceptorRetrofitImpl = it }
        }
    }
}