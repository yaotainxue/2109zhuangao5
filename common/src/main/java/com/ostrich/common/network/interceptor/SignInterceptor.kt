package com.ostrich.common.network.interceptor

import android.util.Log
import com.blankj.utilcode.util.EncryptUtils
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer
import org.json.JSONObject

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:36
 * @Description : SignInterceptor
 */
class SignInterceptor:Interceptor {
    val TAG = "SignInterceptor"


    override fun intercept(chain: Interceptor.Chain): Response {
        //比较规范
//        var request = chain.request()
//        val builder = request.newBuilder()
//
//        //1:获得请求体
//        val body = chain.request().body()
//        val buffer = Buffer()
//        body?.writeTo(buffer)
//        val bodyStr = buffer.toString()//[text={"uName":"zxy","uPwd":"123456","time":"1711414816"}]
//        Log.d(TAG, "intercept: $bodyStr")
//        buffer.close()
//        //2:截取json数据
//        val jsonStr = bodyStr.substring(bodyStr.indexOf("{"),bodyStr.lastIndexOf("}")+1)//{"uName":"zxy","uPwd":"123456","time":"1711414816"}
//        Log.d(TAG, "intercept: $jsonStr")
//        //3:获得键值
//        val sb = StringBuffer()
//        val jsonObject = JSONObject(jsonStr)
//        val keys = jsonObject.keys()
//        while (keys.hasNext()) {
//            val key = keys.next()
//            val value = jsonObject.getString(key)
//            //4:拼接签名
//            sb.append("$key=$value&")
//        }
//        //5:拼接密钥盐
//        sb.append("key=tamboo")
//        Log.d(TAG, "intercept: ${sb.toString()}")
//        //6:MD5加密转小写
//        val sign = EncryptUtils.encryptMD5ToString(sb.toString()).toLowerCase()
//        //7:新的json请求体
//        jsonObject.put("sign",sign)
//        val requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
//            jsonObject.toString())
//        builder.post(requestBody)
//        request = builder.build()

        var request = chain.request()
        val builder = request.newBuilder()

        //1:获得请求体
        val body = chain.request().body()
        val buffer = Buffer()
        body?.writeTo(buffer)
        val bodyStr = buffer.toString()//[text={"uName":"zxy","uPwd":"123456","time":"1711414816"}]
        Log.d(TAG, "intercept: $bodyStr")
        buffer.close()
        //2:截取json数据
        val jsonStr = bodyStr.substring(bodyStr.indexOf("{"),bodyStr.lastIndexOf("}")+1)//{"uName":"zxy","uPwd":"123456","time":"1711414816"}
        Log.d(TAG, "intercept: $jsonStr")
        //3:获得键值
        val sb = StringBuffer()
        val jsonObject = JSONObject(jsonStr)
        val keys = jsonObject.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            val value = jsonObject.getString(key)
            //4:拼接签名
            sb.append(value)
        }
        //5:拼接密钥盐
        sb.append("tamboo")
        Log.d(TAG, "intercept: ${sb.toString()}")
        //6:MD5加密转小写
        val sign = EncryptUtils.encryptMD5ToString(sb.toString()).toLowerCase()
        //7:新的json请求体
        jsonObject.put("sign",sign)
        val requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
            jsonObject.toString())
        builder.post(requestBody)
        request = builder.build()



        return chain.proceed(request)
    }
}