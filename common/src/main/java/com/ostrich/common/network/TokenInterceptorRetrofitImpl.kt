package com.ostrich.common.network

import com.ostrich.common.filed.UrlFiled
import com.ostrich.common.network.interceptor.SignInterceptor
import com.ostrich.common.network.interceptor.TokenInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:28
 * @Description : TokenInterceptorRetrofitImpl
 */
class TokenInterceptorRetrofitImpl:HttpRetrofit {

    override fun getRetrofit(): Retrofit {
        return retrofit!!
    }

    /**
     * 伴生对象：全部都是静态的
     */
    companion object{
        private var retrofit:Retrofit? = null
    }
    class TokenInterceptorHttpRetrofitBuilder{
        private var connectionTimeout:Long = 30*1000
        private var readTimeout:Long = 30*1000
        private var writeTimeout:Long = 30*1000

        fun build():TokenInterceptorRetrofitImpl{
            if(retrofit == null){
                createRetrofit()
            }
            return TokenInterceptorRetrofitImpl()
        }

        private fun createRetrofit() {
            val httpClient = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(TokenInterceptor())
                .connectTimeout(connectionTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(readTimeout,TimeUnit.MILLISECONDS)
                .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS)
                .build()


            val builder = Retrofit.Builder()
                .client(httpClient)
                .baseUrl(UrlFiled.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            retrofit = builder.build()

        }


    }




}