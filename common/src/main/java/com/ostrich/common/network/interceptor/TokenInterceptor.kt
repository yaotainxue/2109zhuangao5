package com.ostrich.common.network.interceptor

import com.blankj.utilcode.util.SPUtils
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:35
 * @Description : TokenInterceptor
 */
class TokenInterceptor:Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        //添加token拦截器
        val request = chain.request().newBuilder()
            .addHeader("token", SPUtils.getInstance().getString("token"))//需要善后
            .build()
        return chain.proceed(request)
    }
}