package com.ostrich.common.network

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:27
 * @Description : HttpType
 */
enum class HttpType {
    SIGN,TOKEN,UPLOAD,ALL
}