package com.ostrich.common.network

import retrofit2.Retrofit

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/25 13:26
 * @Description : HttpRetrofit
 */
interface HttpRetrofit {
    fun getRetrofit():Retrofit
}