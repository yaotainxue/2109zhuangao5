package com.ostrich.demo_brvah;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 09:05
 * @Description : MyAdapter
 */
public class MyAdapter extends BaseQuickAdapter<MyEntity, BaseViewHolder> {
    public MyAdapter(int layoutResId, @Nullable List<MyEntity> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, MyEntity item) {
        helper.setText(R.id.tv, item.getText());
    }
}
