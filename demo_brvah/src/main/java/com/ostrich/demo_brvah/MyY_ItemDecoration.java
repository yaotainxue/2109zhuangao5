package com.ostrich.demo_brvah;

import android.content.Context;
import android.graphics.Color;

import com.yanyusong.y_divideritemdecoration.Y_Divider;
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder;
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 10:01
 * @Description : MyY_ItemDecoration
 */
public class MyY_ItemDecoration extends Y_DividerItemDecoration {
    public MyY_ItemDecoration(Context context) {
        super(context);
    }

    @Nullable
    @Override
    public Y_Divider getDivider(int itemPosition) {
        Y_Divider y_divider = new Y_DividerBuilder()
                .setLeftSideLine(true, Color.BLUE,10,0,0)
                .setTopSideLine(true, Color.RED,10,30,60)
                .setRightSideLine(false, Color.GRAY,10,0,0)
                .setBottomSideLine(true, Color.GREEN,10,0,0)
                .create();
        return y_divider;
    }
}
