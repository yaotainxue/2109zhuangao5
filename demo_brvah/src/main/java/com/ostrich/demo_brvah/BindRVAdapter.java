package com.ostrich.demo_brvah;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 09:13
 * @Description : BindRVAdapter
 */
public class BindRVAdapter {
    @BindingAdapter(value = {"gridEntity"},requireAll = false)
    public static void bindRv(RecyclerView recyclerView, GridEntity gridEntity){
        //获取与recyclerview关联的适配器->null第一次进入;!null数据改变进入
        MyAdapter adapter = (MyAdapter) recyclerView.getAdapter();
        if(adapter == null){
            adapter = new MyAdapter(gridEntity.layout_id,gridEntity.entities);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(),gridEntity.num));
//            recyclerView.addItemDecoration(new MyItemDecoration());
            //添加分割线
            recyclerView.addItemDecoration(new MyY_ItemDecoration(recyclerView.getContext()));
        }else{
            adapter.setNewData(gridEntity.entities);
        }
    }
}
