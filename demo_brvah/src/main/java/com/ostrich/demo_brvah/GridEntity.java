package com.ostrich.demo_brvah;

import java.util.List;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 09:19
 * @Description : GridEntity
 */
public class GridEntity {
    public List<MyEntity> entities;
    public int num;
    public int layout_id;
}
