package com.ostrich.demo_brvah;

/**
 * @Author : yaotianxue
 * @Time : On 2024/3/29 09:04
 * @Description : MyEntity
 */
public class MyEntity {
    public String getText() {
        return text;
    }

    public MyEntity(String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String text;

}
