package com.ostrich.demo_brvah;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.RecoverySystem;

import com.ostrich.demo_brvah.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<MyEntity> mList;
    private ActivityMainBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mList = new ArrayList<>();
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));
        mList.add(new MyEntity("111"));

        GridEntity gridEntity = new GridEntity();
        gridEntity.num = 2;
        gridEntity.layout_id = R.layout.item;
        gridEntity.entities = mList;
        dataBinding.setGridEntity(gridEntity);



    }
}